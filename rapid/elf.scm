;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define mag/0 #x7F)
(define mag/1 #x45)
(define mag/2 #x4C)
(define mag/3 #x46)

(define version/current 1)

(define elf-class/32 1)
(define elf-class/64 2)

(define elf-data/2lsb 1)
(define elf-data/2msb 2)

(define elf-type/none #x0000)
(define elf-type/rel #x0001)
(define elf-type/exec #x0002)
(define elf-type/dyn #x0003)
(define elf-type/core #x0004)
(define elf-type/loos #xfe00)
(define elf-type/hios #xfeff)
(define elf-type/loproc #xff00)
(define elf-type/hiproc #xffff)

(define segment-type/null    #x00000000)
(define segment-type/load    #x00000001)
(define segment-type/dynamic #x00000002)
(define segment-type/interp  #x00000003)
(define segment-type/note    #x00000004)
(define segment-type/shlib   #x00000005)
(define segment-type/phdr    #x00000006)
(define segment-type/loos    #x60000000)
(define segment-type/hios    #x6FFFFFFF)
(define segment-type/loproc  #x70000000)
(define segment-type/hiproc  #x7FFFFFFF)

(define section-type/null     #x00000000)
(define section-type/progbits #x00000001)
(define section-type/symtab   #x00000002)
(define section-type/strtab   #x00000003)
(define section-type/rela     #x00000004)
(define section-type/hash     #x00000005)
(define section-type/dynamic  #x00000006)
(define section-type/note     #x00000007)
(define section-type/nobits   #x00000008)
(define section-type/rel      #x00000009)
(define section-type/shlib    #x0000000A)
(define section-type/dynsym   #x0000000B)
(define section-type/init-array #x0000000E)
(define section-type/fini-array #x0000000F)
(define section-type/preinit-array #x00000010)
(define section-type/group    #x00000011)
(define section-type/symtab-shndx #x00000012)
(define section-type/loos     #x60000000)
(define section-type/hios     #x6FFFFFFF)
(define section-type/loproc   #x70000000)
(define section-type/hiproc   #x7FFFFFFF)
(define section-type/louser   #x80000000)
(define section-type/hiuser   #xFFFFFFFF)

(define section-flag/write #x1)
(define section-flag/alloc #x2)
(define section-flag/execinstr #x4)
(define section-flag/merge #x10)
(define section-flag/strings #x20)
(define section-flag/info-link #x40)
(define section-flag/link-order #x80)
(define section-flag/os-nonconforming #x100)
(define section-flag/group #x200)
(define section-flag/tls #x400)
(define section-flag/maskos #x0FF00000)
(define section-flag/maskproc #xF0000000)

(define symbol-table-name/undef 0)

(define symbol-table-bind/local 0)
(define symbol-table-bind/global 1)
(define symbol-table-bind/weak 2)
(define symbol-table-bind/loos 10)
(define symbol-table-bind/hios 12)
(define symbol-table-bind/loproc 13)
(define symbol-table-bind/hiproc 15)

(define symbol-table-type/notype 0)
(define symbol-table-type/object 1)
(define symbol-table-type/func 2)
(define symbol-table-type/section 3)
(define symbol-table-type/file 4)
(define symbol-table-type/common 5)
(define symbol-table-type/tls 6)
(define symbol-table-type/loos 10)
(define symbol-table-type/hios 12)
(define symbol-table-type/loproc 13)
(define symbol-table-type/hiproc 15)

(define symbol-table-visibility/default 0)
(define symbol-table-visibility/internal 1)
(define symbol-table-visibility/hidden 2)
(define symbol-table-visibility/protected 3)

(define-record-type <elf>
  (make-elf class data)
  elf?
  (class elf-class)
  (data elf-data))

(define (elf-msb? elf)
  (= (elf-data elf) elf-data/2msb))

(define (elf-word-size elf)
  (cond
   ((= (elf-class elf) elf-class/32) 4)
   ((= (elf-class elf) elf-class/64) 8)))

(define (elf-write-integer elf value bytes port)
  (write-bytevector (integer->bytevector value bytes (elf-msb? elf)) port))

(define (elf-write-word elf value port)
  (elf-write-integer elf value (elf-word-size elf) port))

(define (elf-header-size elf)
  (case (elf-word-size elf)
    ((4) 52)
    ((8) 64)))

(define (elf-program-header-size elf)
  (case (elf-word-size elf)
    ((4) #x20)
    ((8) #x38)))

(define (elf-section-header-size elf)
  (case (elf-word-size elf)
    ((4) #x28)
    ((8) #x40)))

(define (elf-write-header elf osabi abi-version type machine entry
			  phoff shoff flags phnum shnum shstrndx port)
  ;; File identification
  (write-bytevector (bytevector mag/0 mag/1 mag/2 mag/3) port)
  ;; File class
  (write-u8 (elf-class elf) port)
  ;; Data encoding
  (write-u8 (elf-data elf) port)
  ;; File version
  (write-u8 version/current port)
  ;; OS/ABI identification
  (write-u8 osabi port)
  ;; ABI version
  (write-u8 abi-version port)
  ;; Pad
  (write-bytevector (make-bytevector 7) port)
  ;; Type
  (elf-write-integer elf type 2 port)
  ;; Machine
  (elf-write-integer elf machine 2 port)
  ;; Version
  (elf-write-integer elf version/current 4 port)
  ;; Entry point
  (elf-write-word elf entry port)
  ;; Program header table
  (elf-write-word elf (if (zero? phnum) 0 phoff) port)
  ;; Section header table
  (elf-write-word elf (if (zero? shnum) 0 shoff) port)
  ;; Flags
  (elf-write-integer elf flags 4 port)
  ;; Header size
  (elf-write-integer elf (elf-header-size elf) 2 port)
  ;; Program header table entry size
  (elf-write-integer elf (if (zero? phnum) 0 (elf-program-header-size elf)) 2 port)
  ;; Program header table entries
  (elf-write-integer elf phnum 2 port)
  ;; Section header table entry size
  (elf-write-integer elf (if (zero? shnum) 0 (elf-section-header-size elf)) 2 port)
  ;; Section header table entries
  (elf-write-integer elf shnum 2 port)
  ;; Section names section header entry index
  (elf-write-integer elf shstrndx 2 port))

(define (elf-write-program-header elf type flags offset vaddr paddr filesz memsz align port)
  (case (elf-word-size elf)
    ((4)
     ;; Type
     (elf-write-integer elf type 4 port)
     ;; Offset
     (elf-write-word elf offset port)
     ;; Virtual address
     (elf-write-word elf vaddr port)
     ;; Physical address
     (elf-write-word elf paddr port)
     ;; Size of segment in the file image
     (elf-write-word elf filesz port)
     ;; Size of segment in memory
     (elf-write-word elf memsz port)
     ;; Flags
     (elf-write-integer elf flags 4 port)
     ;; Alignment
     (elf-write-word elf align port))
    ((8)
     ;; Type
     (elf-write-integer elf type 4 port)
     ;; Flags
     (elf-write-integer elf flags 4 port)
     ;; Offset
     (elf-write-word elf offset port)
     ;; Virtual address
     (elf-write-word elf vaddr port)
     ;; Physical address
     (elf-write-word elf paddr port)
     ;; Size of segment in the file image
     (elf-write-word elf filesz port)
     ;; Size of segment in memory
     (elf-write-word elf memsz port)
     ;; Alignment
     (elf-write-word elf align port))))

(define (elf-write-section-header elf name type flags addr offset size link info
				  addralign entsize port)
  (elf-write-integer elf name 4 port)
  (elf-write-integer elf type 4 port)
  (elf-write-word elf flags port)
  (elf-write-word elf addr port)
  (elf-write-word elf offset port)
  (elf-write-word elf size port)
  (elf-write-integer elf link 4 port)
  (elf-write-integer elf info 4 port)
  (elf-write-word elf addralign port)
  (elf-write-word elf entsize port))

(define (elf-section-header-alignment elf)
  (elf-word-size elf))

(define-record-type <string-table>
  (%make-string-table strings size)
  elf-string-table?
  (strings string-table-strings string-table-set-strings!)
  (size elf-string-table-size string-table-set-size!))

(define (elf-make-string-table)
  (%make-string-table '() 1))

(define (elf-string-table-add! table string)
  (string-table-set-strings! table
			     (cons string (string-table-strings table)))
  (let ((index (elf-string-table-size table)))
    (string-table-set-size! table (+ index (bytevector-length string) 1))
    index))

(define (elf-write-string-table elf table port)
  (write-u8 0 port)
  (do ((strings (reverse (string-table-strings table)) (cdr strings)))
      ((null? strings))
    (write-bytevector (car strings) port)
    (write-u8 0 port)))

(define (elf-symbol-table-entry-size elf)
  (case (elf-word-size elf)
    ((4) 16)
    ((8) 24)))

(define (elf-symbol-table-alignment elf)
  (elf-word-size elf))

(define (elf-write-symbol-table-entry elf name value size bind type visibility shndx port)
  (case (elf-word-size elf)
    ((4)
     (elf-write-integer elf name 4 port)
     (elf-write-word elf value port)
     (elf-write-word elf size port)
     (write-u8 (+ (* 16 bind) type) port)
     (write-u8 visibility port)
     (elf-write-integer elf shndx 2 port))
    ((8)
     (elf-write-integer elf name 4 port)
     (write-u8 (+ (* 16 bind) type) port)
     (write-u8 visibility port)
     (elf-write-integer elf shndx 2 port)
     (elf-write-word elf value port)
     (elf-write-word elf size port))))

(define (elf-rela-table-alignment elf)
  (elf-word-size elf))

(define (elf-write-rela-table-entry elf offset sym type addend port)
  (let ((info
	 (case (elf-word-size elf)
	   ((4)
	    (+ (* (expt 2 8) sym) type))
	   ((8)
	    (+ (* (expt 2 32) sym) type)))))
    (elf-write-word elf offset port)
    (elf-write-word elf info port)
    (elf-write-word elf addend port)))

(define (elf-rela-table-entry-size elf)
  (case (elf-word-size elf)
    ((4) 12)
    ((8) 24)))
