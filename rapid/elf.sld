;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Elf files.

(define-library (rapid elf)
  (export make-elf elf?

          elf-make-string-table
          elf-string-table?
          elf-string-table-add!
          elf-string-table-size

          elf-write-integer
          elf-write-word

          elf-write-header
          elf-write-program-header
          elf-write-section-header
          elf-write-symbol-table-entry
          elf-write-rela-table-entry
          elf-write-string-table

          elf-header-size
          elf-program-header-size
          elf-section-header-size
          elf-symbol-table-entry-size
          elf-rela-table-entry-size
          elf-symbol-table-alignment
          elf-section-header-alignment
          elf-rela-table-alignment

          elf-class/32 elf-class/64
          elf-data/2lsb elf-data/2msb
          elf-type/none elf-type/rel elf-type/exec elf-type/dyn
          elf-type/core elf-type/loos elf-type/hios elf-type/loproc
          elf-type/hiproc

          segment-type/null
          segment-type/load
          segment-type/dynamic
          segment-type/interp
          segment-type/note
          segment-type/shlib
          segment-type/phdr
          segment-type/loos
          segment-type/hios
          segment-type/loproc
          segment-type/hiproc

          section-type/null
          section-type/progbits
          section-type/symtab
          section-type/strtab
          section-type/rela
          section-type/hash
          section-type/dynamic
          section-type/note
          section-type/nobits
          section-type/rel
          section-type/shlib
          section-type/dynsym
          section-type/init-array
          section-type/fini-array
          section-type/preinit-array
          section-type/group
          section-type/symtab-shndx
          section-type/loos
          section-type/hios
          section-type/loproc
          section-type/hiproc
          section-type/louser
          section-type/hiuser

	  section-flag/write
	  section-flag/alloc
          section-flag/execinstr
          section-flag/merge
          section-flag/strings
          section-flag/info-link
          section-flag/link-order
          section-flag/os-nonconforming
          section-flag/group
          section-flag/tls
          section-flag/maskos
          section-flag/maskproc

          symbol-table-name/undef

          symbol-table-bind/local
          symbol-table-bind/global
          symbol-table-bind/weak
          symbol-table-bind/loos
          symbol-table-bind/hios
          symbol-table-bind/loproc
          symbol-table-bind/hiproc
          symbol-table-type/notype
          symbol-table-type/object
          symbol-table-type/func
          symbol-table-type/section
          symbol-table-type/file
          symbol-table-type/common
          symbol-table-type/tls
          symbol-table-type/loos
          symbol-table-type/hios
          symbol-table-type/loproc
          symbol-table-type/hiproc
          symbol-table-visibility/default
          symbol-table-visibility/internal
          symbol-table-visibility/hidden
          symbol-table-visibility/protected)
  (import (scheme base)
          (rapid integer))
  (include "elf.scm"))
